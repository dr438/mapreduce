import java.io.IOException;
import java.util.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class BlockPageRank {
    public static class BPRCounter{
        public static enum BPR_COUNTER{
            NUM_ITERATIONS,
            NUM_SINKS,
          NUM_EDGES,
          NUM_NODES,
          NUM_BLOCKS,
          RESIDUAL_PR
        };
    }

    public static class BPRMapper extends Mapper<Object, Text, Text, Text>{
    private Text reducerkey = new Text();
    private Text result = new Text();

/* Steps done in Mapper: 
    1. Increment the Hadoop Counter for number of nodes, number of edges, number of sink nodes
    2. The record is read in TextInputFormat, where key is the bit-offset and value is the one text line
    3. Split the value to retrieve - nodeid_blockid pagerank outgoing_links(if any)
    4. Emit Key:<blockId> Value: PR:<srcNodeId_blockId> <PR> <outgoing>  
    5. If the outgoing node belongs to the block, 
        Emit: <blockId> Value: BE:<srcNodeId destNodeID PR/degree(srcNodeId)> 
    6. If the outgoing node doesn't belong to the block,
        Emit: <blockId(dest)> Value: BC:<srcNodeId destNodeId PR/degree(srcNodeId)>
    Note: vals[0] = nodeid_blockid
          vals[1] = pagerank
          vals[2] = outgoing_links, in nodeid_blockid format
*/
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            context.getCounter(BPRCounter.BPR_COUNTER.NUM_NODES).increment(1);                    //Step 1
            String[] vals = value.toString().split(" ");                                          //Step 3
            String[] src_blk_id = vals[0].split("_");
            String srcNodeId = src_blk_id[0].trim();
            String blockId = src_blk_id[1].trim();
            String strEmit1 = "PR:" + vals[0];
              
            if(vals.length==2) {
                strEmit1 += " " + vals[1];
                context.getCounter(BPRCounter.BPR_COUNTER.NUM_SINKS).increment(1);                //Step 1
            } else if (vals.length==3) {
                strEmit1 += " " + vals[1]+ " " + vals[2];
            }  else {
                //Ill formatted string.... 
            }
            reducerkey.set(blockId);
            result.set(strEmit1);
            context.write(reducerkey,result);                                                    //Step 4

            if(vals.length==3) {
                context.getCounter(BPRCounter.BPR_COUNTER.NUM_EDGES).increment(1);
                String[] outgoing = vals[2].split(",");
                Double pr_emit = (Double.parseDouble(vals[1].trim()))/outgoing.length;
                for(int idx=0;idx<outgoing.length;idx++) {
                    String[] node_blk = outgoing[idx].split("_");
                    if(node_blk[1].equals(blockId)) {
                        String strPR = String.format("%.10f", pr_emit);
                        String strEmit2 = "BE:" + vals[0] + " " + outgoing[idx] + " " + strPR;
                        reducerkey.set(blockId);
                        result.set(strEmit2);                                                          //Step 5
                    } else {
                        String strPR = String.format("%.10f", pr_emit);
                        String strEmit3 = "BC:" + vals[0] + " " + outgoing[idx] + " " +  strPR;
                        reducerkey.set(node_blk[1]);
                        result.set(strEmit3);                                                          //Step 6
                    }
                    context.write(reducerkey,result);
                }
            }
        }
    }

    public static class BPRReducer extends Reducer<Text,Text,Text,Text> {

        private Text result = new Text();
        private Text finalkey = new Text();
/* Steps done in Reducer: 
    1. Iterate over all values received for Node. Three Possible tags: PR, BE, BC
    2. If val has PR tag, create a node obj and store in blkNodes HashMap with nodeId as key.
    3. If val has BE tag, extract the src & dst node id and store in BE HashMap with destNodeId as key.
    4. If val has BC tag, extract page rank & accumulate the pagerank in BC HashMap with dst node_id as key.
    5. Use the IterateBlockOnce algorithm given in the project description. 
        Note: 1. IterateBlockOnce runs upto a max of 20 iterations or till residual<0.001 
              2. Pr(t) is set at the start of every IterateBlockOnce iteration 
              3. Residual of IterateBlockOnce computed using |PR(t)-PR(t+1)|/PR(t+1)
              4. Increment Hadoop counters at every iteration
    6. Compute the block residual using |PR(start)-PR(end)|/PR(end)
    7. Add the block residual to the Hadoop Counter
    8. EMIT <"",key pr(end) outgoinglinks>  (Write to the output file for next iteration)
*/  
        public void reduce(Text key1, Iterable<Text> values,Context context) throws IOException, InterruptedException {      
            context.getCounter(BPRCounter.BPR_COUNTER.NUM_BLOCKS).increment(1);
            Double d = 0.85; //damping factor
            Double pr_t=0.0;
            int i = 0;
            int degree = 0;
            Double pr_new = 0.0;
            Double epsilon=0.001;
            Double curr_residual=0.0;
            Double block_avg_residual=0.0;
            HashMap<String,Node> blkNodes = new HashMap<>();
            HashMap<String,ArrayList<String>> BE = new HashMap<String,ArrayList<String>>();
            HashMap<String,Double> BC = new HashMap<>();
            // Used to find the 2 minimum keys in the block and to display their page rank
            String minKeyStr="";
            int minKey=685229;
            String minKeyStr1="";
            int minKey1=685229;

            for(Text val: values) {                                                     //Step 1
                String strVal = val.toString();
                String[] outNodes;
                String[] PRString = strVal.trim().split(":");
                String[] PRvalue = PRString[1].trim().split(" ");
                String[] src_blk_id = PRvalue[0].split("_");
                String srcNodeId = src_blk_id[0].trim();
                Node nodeObj=null; 
                if (strVal.contains("PR:")) {               //Step 2: Value: PR: <srcNodeId> <PR> <outgoing> 
                    pr_t = Double.parseDouble(PRvalue[1].trim());
                    if(PRvalue.length==3) {
                        outNodes = PRvalue[2].split(",");
                        degree = outNodes.length;
                        nodeObj = new Node(pr_t, PRvalue[2],degree,Integer.parseInt(src_blk_id[1]));
                    } else if(PRvalue.length==2) {
                        nodeObj = new Node(pr_t,Integer.parseInt(src_blk_id[1]));
                    } 
                    blkNodes.put(srcNodeId,nodeObj);
                } else if(strVal.contains("BE:")) {      //Step 3: Value: BE:<srcNodeId destNodeID PR/degree(srcNodeId)> 
                    String[] dest_blk_id = PRvalue[1].split("_");
                    String destNodeID = dest_blk_id[0].trim();
                    if(BE.containsKey(destNodeID)) {
                        ArrayList<String> srcNodeList = BE.get(destNodeID);
                        srcNodeList.add(srcNodeId); 
                        BE.put(destNodeID,srcNodeList);
                    } else {
                        ArrayList<String> srcNodeList = new ArrayList<String>();  
                        srcNodeList.add(srcNodeId);
                        BE.put(destNodeID,srcNodeList);
                    }
                } else if(strVal.contains("BC:")) {    //Step 4: Value: BC:<srcNodeId destNodeId PR/degree(srcNodeId)>
                    String[] dest_blk_id = PRvalue[1].split("_");
                    String destNodeID = dest_blk_id[0].trim();
                    if(BC.containsKey(destNodeID)) {
                        Double destPR = BC.get(destNodeID);
                        destPR += Double.parseDouble(PRvalue[2].trim());
                        BC.put(destNodeID,destPR);
                    } else {
                        Double destPR = Double.parseDouble(PRvalue[2].trim());
                        BC.put(destNodeID,destPR);
                    }
                }
            }
            //Step 5
            do { 
                curr_residual=0.0;
                /* Step 5.2 & Compute 1st min nodeId in this for-loop */
                for(String key: blkNodes.keySet()) {
                    Node node = blkNodes.get(key);
                    node.setPRT(node.getPageRank());
                    if(Integer.parseInt(key)<minKey) {
                        minKey = Integer.parseInt(key);
                        minKeyStr = key;
                    }
                }
                for (String key : blkNodes.keySet()) {
                    Node node = blkNodes.get(key);
                    pr_new =0.0;
                    if(BE.containsKey(key)) {
                        ArrayList<String> BENodeList = BE.get(key);
                        for(String nodeId1: BENodeList) {
                            Node beNode = blkNodes.get(nodeId1);
                            pr_new += beNode.getPRT()/beNode.getDegree();
                        }
                    }
                    if(BC.containsKey(key)) {
                        pr_new += BC.get(key);  
                    }
                    pr_new = (((1.0-d)/685230.0) + ((d*pr_new)));
                    curr_residual += (Math.abs(node.getPageRank()-pr_new))/pr_new;    
                    node.setPageRank(pr_new); 
                    blkNodes.put(key,node);
                } 
            block_avg_residual = curr_residual/blkNodes.size();
            i++;
            context.getCounter(BPRCounter.BPR_COUNTER.NUM_ITERATIONS).increment(1);
        } while(epsilon<block_avg_residual && i<20);
        System.out.println("Reduce: Block:" + key1.toString() + " No of Iterations: " + i + " block_avg_residual: " + block_avg_residual);
 
        //Use this for-loop to find the 2nd min nodeId in the block     
        for(String key: blkNodes.keySet()) {
            if(Integer.parseInt(key)>minKey && Integer.parseInt(key)<minKey1) {
                minKey1 = Integer.parseInt(key);
                minKeyStr1 = key;
            }
            Node node = blkNodes.get(key);
            Double pr_start=node.getPRStart();
            Double pr_end = node.getPageRank();
            Double residual = (Math.abs(pr_start - pr_end))/pr_end;
            long  counter_pr = (long) (residual * 10000);
            context.getCounter(BPRCounter.BPR_COUNTER.RESIDUAL_PR).increment(counter_pr); 
            String strResult = "";
            if(node.degree!=0) {
                strResult = key + "_" + node.getBlockId()+ " " + String.format("%.10f", pr_end) + " " +node.adjList;
            } else {
                strResult = key + "_" + node.getBlockId()+ " " + String.format("%.10f", pr_end);
            }
            finalkey.set("");
            result.set(strResult);
            context.write(finalkey,result);
            }
            //Printing the PageRank of First 2 nodes in the block: 
            System.out.println("Reduce:Block:"+key1.toString()+" PageRank of Node:" + minKey + " PageRank:" + blkNodes.get(minKeyStr).getPageRank());
            System.out.println("Reduce:Block:"+key1.toString()+" PageRank of Node:" + (minKey1) + " PageRank:" + blkNodes.get(Integer.toString(minKey1)).getPageRank());
        }              
    }

/*
Usage: hadoop jar BlockPageRank.jar BlockPageRank s3://amazonemrlsi/input s3://amazonemrlsi/output 
Instructions:
1. Place your files in s3://<bucket_path>/input/filtered_edges.txt  
    Note: Program reads all files placed in the input bucket
2. The output directory becomes the input to next iteration 
*/

    public static void main(String[] args) throws Exception {
        double epsilon=0.001;   //Termination Criteria
        int idx=0;
        double avg_residual=0.0;
        double avg_iterations=0.0;
        /* Compute PageRank till Average Residual < epsilon */
        do {
            String inputPath="";
            String outputPath="";
            Configuration conf = new Configuration();
            Job job = Job.getInstance(conf, "BlockPageRank");
            job.setJarByClass(BlockPageRank.class);
            job.setMapperClass(BPRMapper.class);
            job.setReducerClass(BPRReducer.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            if(idx==0) {
                inputPath=args[0];
                outputPath=args[1]+(idx)+"/";
            } else {
                inputPath=args[1]+(idx-1)+"/";
                outputPath=args[1]+idx+"/";
            }
            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(outputPath));
            try {
                job.waitForCompletion(true);
            } catch(Exception e) {
                System.out.println("BlockPageRank Exception in Iteration:" + idx);
            }
      
            //Access the counters & pass on the residual page rank to next iteration... 
            //Access counter values for both num_nodes and lost pagerank
            Counters counters = job.getCounters();
            Counter numNodes = counters.findCounter(BPRCounter.BPR_COUNTER.NUM_NODES);
            Counter residualPR = counters.findCounter(BPRCounter.BPR_COUNTER.RESIDUAL_PR);
            Counter numBlocks = counters.findCounter(BPRCounter.BPR_COUNTER.NUM_BLOCKS);
            Counter numSinks = counters.findCounter(BPRCounter.BPR_COUNTER.NUM_SINKS);
            Counter numEdges = counters.findCounter(BPRCounter.BPR_COUNTER.NUM_EDGES);
            Counter numIterations = counters.findCounter(BPRCounter.BPR_COUNTER.NUM_ITERATIONS);
            System.out.println("Iteration Summary: ");
            System.out.println("No of Nodes: " + numNodes.getValue() + " Residual PR: " + residualPR.getValue());
            System.out.println("No of Edges: " + numEdges.getValue() + " Sink Nodes: " + numSinks.getValue());
            System.out.println("Total No of Iterations across Blocks:" + numIterations.getValue());
            avg_iterations = (numIterations.getValue()/(double)numBlocks.getValue());
            avg_residual = (residualPR.getValue()/10000.0)/numNodes.getValue(); 
            String residual = String.format("%.10f", avg_residual);
            String iterations = String.format("%.10f", avg_iterations);
            System.out.println("Iteration: " + idx+ " Average Residual: " + residual); 
            System.out.println("Iteration: " + idx + " Average Iterations: " + iterations); 
            idx++; 
        } while(epsilon<avg_residual);
    System.exit(0);
  }
}
