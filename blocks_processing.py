#!/usr/bin/python 

blocks_list = []
no = 0
with open('blocks.txt','r') as f:
	for line in f:
		no += int(line.strip())
		blocks_list.append(no-1)
print (blocks_list)

'''
get_blockId: Binary search on blocks_list to return index as block id
'''
def get_blockId(nodeId):
	start=0
	end = len(blocks_list)
	
	while(start<=end):
		mid = int((end+start)/2)
		if (mid==0 and blocks_list[mid]>nodeId):
			return mid
		elif (blocks_list[mid]==nodeId):
			return mid
		elif (blocks_list[mid]>nodeId and blocks_list[mid-1]<nodeId):
			return mid
		elif (blocks_list[mid]<nodeId and blocks_list[mid+1]>nodeId):
			return mid+1
		elif (blocks_list[mid]>nodeId):
			end = mid-1
		else:	
			start = mid+1

fromNetID = 0.834
#fromNetID = 0.556
rejectMin = 0.9 * fromNetID
rejectLimit = rejectMin + 0.01

print (rejectMin)
print (rejectLimit)

def selectInputLine(x):
	if ((x >= rejectMin) and (x < rejectLimit)):
		return False
	else:
		return True 

count = 0
adjDict = {}
with open('edges.txt','r') as f:
	for line in f:
		vals = line.split()
		if(len(vals)==3):
			if(selectInputLine(float(vals[2]))):
				count = count+1
				if int(vals[0]) not in adjDict.keys():
					adjList = [int(vals[1])]
					adjDict[int(vals[0])] = adjList
				else:
					adjDict[int(vals[0])].append(int(vals[1]))
print (count)

print (blocks_list[-1])

fp = open('filtered_edges.txt','a')

for idx in range(0,blocks_list[-1]+1):
	writeStr=""
	if idx not in adjDict.keys():
		blockid = get_blockId(idx)
		writeStr = str(idx)+"_" +str(blockid) + " 0.00000145936614" + "\n"
	else:
		blockid = get_blockId(idx)
		adjStr = ""
		adjList = adjDict[idx]
		for idx1 in range(0,len(adjList)):
			adjblockid=get_blockId(adjList[idx1])
			adjStr += str(adjList[idx1]) + "_"+str(adjblockid) + ","
		writeStr = str(idx) + "_"+str(blockid) + " 0.00000145936614 " + str(adjStr[:-1]) + "\n"
	fp.write(writeStr)
fp.close()
