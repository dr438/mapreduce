import java.io.IOException;
import java.util.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class Node {
    public double pageRank; //PageRank of the node - set after every IterateBlockOnce loop 
    public String adjList;  //AdjList of the node - Use the same string to write to file 
    public int degree;      //Degree - no. of outgoing links
    public int blockid;     //BlockId of the node
    public double pr_start; //PageRank received at the start of reducer pass
    public double pr_t;     //PR(t) set at the beginning of IterateBlockOne 

    public Node(double pageRank, String adjList, int degree,int blockid) {
        this.pageRank = pageRank;
        this.pr_start = pageRank;
        this.pr_t   = pageRank;
        this.adjList = adjList;
        this.degree = degree;
        this.blockid = blockid;
    }

    public Node(double pageRank, int blockid) {
        this.pageRank = pageRank;
        this.pr_start = pageRank;
        this.blockid = blockid;
        this.degree = 0;
        this.adjList= "";
    }
    
    public double getPageRank() {
        return pageRank;
    }

    public int getBlockId() {
        return blockid;
    }
    
    public void setPageRank(double pageRank) {
        this.pageRank = pageRank;
    }

    public String getAdjList() {
        return adjList;
    }
    
    public void setAdjList(String adjList) {
        this.adjList = adjList;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public int getDegree() {
        return degree;
    }
  
    public double getPRStart() {
        return pr_start;
    }
 
    public void setPRT(double prt){
        pr_t = prt;
    }
    
    public double getPRT() {
        return pr_t;
    }
}