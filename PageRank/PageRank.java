import java.io.IOException;
import java.util.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class PageRank {

    public static class PRCounter {
        public static enum PR_COUNTER {
            NUM_SINKS,
            NUM_EDGES,
            NUM_NODES,
            RESIDUAL_PR
        };
    }

    public static class PRMapper
       extends Mapper<Object, Text, Text, Text> {
    
    private Text srcNodeId = new Text();
    private Text result = new Text();

/* Steps done in Mapper: 
    1. Increment the Hadoop Counter for number of nodes, number of edges, number of sink nodes
    2. The record is read in TextInputFormat, where key is the bit-offset and value is the one text line
    3. Split the value to retrieve - nodeid_blockid pagerank outgoing_links(if any)
    4. If no outgoing links, EMIT <(nodeid_block_id),A:PageRank>
    5. If there are outgoing links, EMIT <(node_id,block_id) A:PageRank outgoing_links> 
    6. If there are outgoing links, EMIT <(node_id,block_id) P:PageRank/degree(node_id)>

    Note: vals[0] = nodeid_blockid
          vals[1] = pagerank
          vals[2] = outgoing_links, in nodeid_blockid format
*/
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            context.getCounter(PRCounter.PR_COUNTER.NUM_NODES).increment(1);            //Step 1
            String[] vals = value.toString().split(" ");                                //Step 2 & 3
            if (vals.length>2) { 
                srcNodeId.set(vals[0].trim());
                double pr = Double.parseDouble(vals[1].trim());
                String[] adjList = vals[2].split(",");
                String strAdjList = "A:";
                result.set(strAdjList + vals[1] + " " + vals[2]);   
                context.write(srcNodeId,result);                                        //Step 5                                         
                double updatedPR = pr/adjList.length ;
                for(int idx=0;idx<adjList.length;idx++) {
                    String strUpdatedPR = "P:" + Double.toString(updatedPR);
                    srcNodeId.set(adjList[idx]);
                    result.set(strUpdatedPR);
                    context.write(srcNodeId,result);                                    //Step 6
                    context.getCounter(PRCounter.PR_COUNTER.NUM_EDGES).increment(1);    //Step 1
                }
            } else if (vals.length==2) {      /* No outgoing links */
              context.getCounter(PRCounter.PR_COUNTER.NUM_SINKS).increment(1);          //Step 1
              srcNodeId.set(vals[0].trim());
              result.set("A:" + vals[1].trim());                                        
              context.write(srcNodeId,result);                                          //Step 4
            }      
        }
    }

    public static class PRReducer extends Reducer<Text,Text,Text,Text> {
        private Text result = new Text();
        private Text finalkey = new Text();
/* Steps done in Reducer: 
    1. Iterate over all values received for Node. Two Possible tags: A or P
    2. If val has P tag, add it to the total incoming pageranks (pr_sum)
    3. If val has A tag, it could either contain only (pagerank) or (pagerank and outgoing links)
    4. If val with A tag, set pr(t)=pagerank and if any outgoing links , set it to adjList.
    5. Compute the PageRank(t+1) = (1-d)/N + d*(pr_sum)
    6. Compute the residual using |PR(t)-PR(t+1)|/PR(t+1)
    7. Add the residual to the Hadoop Counter
    8. EMIT <"",key pr(t+1) outgoinglinks>  (Write to the output file for next iteration)
*/

        public void reduce(Text key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
            Double d = 0.85;            //damping factor
            Double pr_sum=0.0;          //PageRank_sum of all the incoming page ranks
            String adjList = "";        //Retrieve adjacency list to be written into output file
            Double pr_new = 0.0;        //PageRank after using damping factor (i.e., PageRank at t+1)
            Double pr_t = 0.0;          //PageRank at time t
            Double residual = 0.0;
            long counter_pr = 0;
            String strResult = "";
            for(Text val: values) {
                String strVal = val.toString();
                if (strVal.contains("A:")) {
                    String[] pr_adjList = strVal.split(":");
                    String[] vals = pr_adjList[1].split(" ");
                    pr_t = Double.parseDouble(vals[0].trim());                              //Step 4
                    if(vals.length==2) {                
                        adjList = " " + vals[1];
                    } 
                } else if(strVal.contains("P:")) {
                    String[] vals = strVal.split(":");  
                    pr_sum += Double.parseDouble(vals[1]);                                  //Step 2 
                }
            }
            
            pr_new = (((1.0-d)/685230.0) + ((d*pr_sum)));                                   //Step 5
            residual = (Math.abs(pr_t - pr_new))/pr_new;                                    //Step 6
            counter_pr = (long) (residual * 10000);                    
            context.getCounter(PRCounter.PR_COUNTER.RESIDUAL_PR).increment(counter_pr);     //Step 7
            strResult = key.toString() + " " + String.format("%.10f", pr_new) + adjList;
            finalkey.set("");
            result.set(strResult);
            context.write(finalkey,result);                                                 //Step 8
        }
    }

/*
Usage: hadoop jar PageRank.jar PageRank s3://amazonemrlsi/input s3://amazonemrlsi/output 
Instructions:
1. Place your files in s3://<bucket_path>/input/filtered_edges.txt  
    Note: Program reads all files placed in the input bucket
2. The output directory becomes the input to next iteration 
*/
    public static void main(String[] args) throws Exception {
        for(int idx=0;idx<5;idx++) {
            System.out.println("Iteration:" + idx);
            String inputPath="";
            String outputPath="";
            Configuration conf = new Configuration();
            Job job = Job.getInstance(conf, "PageRank");
            job.setJarByClass(PageRank.class);
            job.setMapperClass(PRMapper.class);
            job.setReducerClass(PRReducer.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            if(idx==0) {
                inputPath=args[0];
                outputPath=args[1]+(idx)+"/";
            } else {
                inputPath=args[1]+(idx-1)+"/";
                outputPath=args[1]+idx+"/";
            }
            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(outputPath));
            try {
                job.waitForCompletion(true);
            } catch(Exception e) {
                System.out.println("PageRank Exception in Iteration:" + idx);
            }

            //Access the counters & pass on the residual page rank to next iteration... 
            //Access counter values for both num_nodes and lost pagerank
            Counters counters = job.getCounters();
            Counter numNodes = counters.findCounter(PRCounter.PR_COUNTER.NUM_NODES);
            Counter residualPR = counters.findCounter(PRCounter.PR_COUNTER.RESIDUAL_PR);
            Counter numEdges = counters.findCounter(PRCounter.PR_COUNTER.NUM_EDGES);
            Counter numSinks = counters.findCounter(PRCounter.PR_COUNTER.NUM_SINKS);
            System.out.println("No of Nodes: " + numNodes.getValue() + " Residual PR: " + residualPR.getValue());
            System.out.println("No of Edges: " + numEdges.getValue() + " Sink Nodes: " + numSinks.getValue());
            double avg_residual = (residualPR.getValue()/10000.0)/numNodes.getValue(); 
            String residual = String.format("%.10f", avg_residual);
            System.out.println("Average Residual: " + residual);   
        }
        System.exit(0);
    }
}

