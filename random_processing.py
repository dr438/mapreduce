#!/usr/bin/python 

'''
get_blockId: Assign a random blockId for each node 
	Using a identity hash function.. h(nodeId) = nodeId
	return nodeId%68;
'''
def get_blockId(nodeId):
	return (nodeId%68)

fromNetID = 0.834
#fromNetID = 0.556
rejectMin = 0.9 * fromNetID
rejectLimit = rejectMin + 0.01

print (rejectMin)
print (rejectLimit)

def selectInputLine(x):
	if ((x >= rejectMin) and (x < rejectLimit)):
		return False
	else:
		return True 

count = 0
adjDict = {}
with open('edges.txt','r') as f:
	for line in f:
		vals = line.split()
		if(len(vals)==3):
			if(selectInputLine(float(vals[2]))):
				count = count+1
				if int(vals[0]) not in adjDict.keys():
					adjList = [int(vals[1])]
					adjDict[int(vals[0])] = adjList
				else:
					adjDict[int(vals[0])].append(int(vals[1]))
print (count)


fp = open('random_filtered_edges.txt','a')

for idx in range(0,685230):
	writeStr=""
	if idx not in adjDict.keys():
		blockid = get_blockId(idx)
		writeStr = str(idx)+"_" +str(blockid) + " 0.00000145936614" + "\n"
	else:
		blockid = get_blockId(idx)
		adjStr = ""
		adjList = adjDict[idx]
		for idx1 in range(0,len(adjList)):
			adjblockid=get_blockId(adjList[idx1])
			adjStr += str(adjList[idx1]) + "_"+str(adjblockid) + ","
		writeStr = str(idx) + "_"+str(blockid) + " 0.00000145936614 " + str(adjStr[:-1]) + "\n"
	fp.write(writeStr)
fp.close()
